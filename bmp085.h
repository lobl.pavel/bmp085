#ifndef BMP085_H

#define BMP085_ADDRESS          0x77
#define BMP085_OK               0
#define BMP085_BUS_ERROR        1
#define BMP085_INVAL_DEV        2

#include <stdint.h>

enum bmp085_mode {
    BMP085_LOW_PWR = 0,
    BMP085_STD = 1,
    BMP085_HIGH_RES = 2,
    BMP085_ULTRA_HIGH_RES = 3
};

struct bmp085_calib_coefs {
    int16_t ac1, ac2, ac3;
    uint16_t ac4, ac5, ac6;
    int16_t b1, b2;
    int16_t mb, mc, md;
};

struct bmp085 {
    int fd;
    struct bmp085_calib_coefs coefs;
    enum bmp085_mode mode;
};

int bmp085_init(struct bmp085 *bmp, const char *dev);
void bmp085_set_mode(struct bmp085 *bmp, enum bmp085_mode mode);

int bmp085_read_temp(struct bmp085 *bmp, int *t);
int bmp085_read_press(struct bmp085 *bmp, int *press, int *temp);

float bmp085_press_to_sea(float press, float alt);
float bmp085_press_to_altitude(float sea, float press);

#endif
