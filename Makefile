CC=gcc
CFLAGS=-Wall
LDFLAGS=-L . -lm
LDLIBS=

PREFIX = $(DESTDIR)/usr/local
BINDIR = $(PREFIX)/bin
LIBDIR = $(PREFIX)/lib
INCLUDEDIR = $(PREFIX)/include

bmp085:
	$(CC) $(CFLAGS) -fPIC -o libbmp085.so bmp085.c -shared
	$(CC) $(CFLAGS) -c bmp085.c
	ar -rcs bmp085.a bmp085.o

	$(CC) $(CFLAGS) $(LDFLAGS) bmp085-get.c -lbmp085 -o bmp085-get

clean:
	rm -rf bmp085-get bmp085-get.o libbmp085.so bmp085.a bmp085.o 2>/dev/null

install:
	install -D bmp085-get $(DESTDIR)$(BINDIR)/bmp085-get
	install -D libbmp085.so $(DESTDIR)$(LIBDIR)/libbmp085.so
	install -D bmp085.h $(DESTDIR)$(INCLUDEDIR)/bmp085.h
	ldconfig $(DESTDIR)$(LIBDIR)

uninstall:
	-rm $(DESTDIR)$(BINDIR)/bmp085-get
	-rm $(DESTDIR)$(LIBDIR)/libbmp085.so
	-rm $(DESTDIR)$(INCLUDEDIR)/bmp085.h
